package org.example;

public class TypeErasure {

    public class Node<T> {

        private T data;
        private Node<T> prev;
        private Node<T> next;

        public Node(T data, Node<T> prev, Node<T> next) {
            this.data = data;
            this.prev = prev;
            this.next = next;
        }

        public T getData() { return data; }
    }

}

