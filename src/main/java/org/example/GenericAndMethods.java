package org.example;

import java.math.BigDecimal;

public class GenericAndMethods {
    public static void main(String[] args) {
        String heh = "Heh";
        String[] names = {heh, "Paule"};
        Character[] characters = {'A', 'B', 'C'};
        Integer[] integers = {1, 2, 4, 5, 5};

        print(names);
        print(characters);
        print(integers);

        print("heh");
        print(9);
        print(1L);
        print(new BigDecimal("20"));
    }

    static <T> void print(T[] array) {
        for (T e : array) {
            System.out.println( e.getClass().getName() + " - " + e );
        }
        System.out.println();
    }

    static <T> void print(T item) {
        System.out.println( item.getClass().getName() + " - " + item );
        System.out.println();
    }

}
