package org.example.wildcard;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class UpperBoundedWildcards {
    public static void main(String[] args) {
        List<Double> doubles = Arrays.asList(1.9, 1.3);
        List<Integer> integers = Arrays.asList(1, 2);
        List<Number> numbers = Arrays.asList(5, 7f, Math.sqrt(5), 1L, new BigDecimal("10.56"));
        printNumbers(doubles);
        printNumbers(integers);
        printNumbers(numbers);
    }

    static void printNumbers(List<? extends Number> list) {
        list.forEach(e -> System.out.println(e.getClass().getName() + ": " + e));
        System.out.println();
    }
}
