package org.example.wildcard;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class LowerBoundedWildcards {
    public static void main(String[] args) {
        List<Integer> integers = Arrays.asList(1, 2);
        List<Number> numbers = Arrays.asList(5, 7f, Math.sqrt(5), 1L, new BigDecimal("10.56"));
        printNumbers(integers);
        printNumbers(numbers);
    }

    static void printNumbers(List<? super Integer> list) {
        list.forEach(e -> System.out.println(e.getClass().getName() + ": " + e));
        System.out.println();
    }
}
