package org.example.wildcard;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class UnboundedWildcards {
    public static void main(String[] args) {
        List<Object> objects = Arrays.asList("3.14", 2, new Date());
        List<Integer> integers = Arrays.asList(1, 2);
        List<String> strings = Arrays.asList("1", "fun");
        print(objects);
        print(integers);
        print(strings);
    }

    static void print(List<?> list) {
        list.forEach(e -> System.out.println(e.getClass().getName() + ": " + e));
        System.out.println();
    }
}
