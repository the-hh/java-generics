package org.example;

import java.util.Arrays;

public class BoundedTypeParameter {
    public static void main(String[] args) {
        Integer[] numbers = {-1, 1, 2, 3, -9, 10};
        System.out.println( countGreaterThan(numbers, 0) );

        Double[] numbers2 = {1.0, 2.0, -3.8};
        System.out.println( countGreaterThan(numbers2, 0.9) );
    }

    static <T extends Comparable<T>> int countGreaterThan( T[] numbers, T number) {
        return (int) Arrays.stream(numbers)
                .filter(n -> n.compareTo(number) > 0)
                .count();
    }

}
