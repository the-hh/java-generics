package org.example.genericsAndClasses;

public class Container<T> {
    
    private T content;
    private String name;

    public void set(T content) {
        this.content = content;
    }

    public T get() {
        return this.content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
