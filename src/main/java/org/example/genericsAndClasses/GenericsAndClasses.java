package org.example.genericsAndClasses;

public class GenericsAndClasses {
    public static void main(String[] args) {
        Container<Phone> pocket = new Container<>();
        pocket.set(new Phone("Sony"));
        pocket.setName("Phone");
        System.out.println(pocket.get());
        System.out.println(pocket.getName());
        System.out.println(pocket.get().getBrand());
        System.out.println("--------------------------");

        Container<Letter> mailbox = new Container<>();
        mailbox.set(new Letter("Bill", "bill content"));
        mailbox.setName("Letter (bill)");
        System.out.println(mailbox.get());
        System.out.println(mailbox.getName());
        System.out.println(mailbox.get().getSender() + ": " + mailbox.get().getContent());
    }
}
